<?php

Class ThreadsController extends Controller {

	private $ThreadsRepository;

	public function __construct(){
		$this->threadsRepository = new ThreadsRepository();
	}

	public function defaultAction(){
		self::listAction();
	} 

	public function newThread(){
		require "Views/Threads/newThread.php";
	}

	public function createThread(){
		if($_SERVER["REQUEST_METHOD"] == "POST"){
			$thread = new Threads(NULL, $_POST["name"], $_POST["description"], $_POST["category"});
			$status = $this ->threadsRepository->insert($thread);
			require "Views/Threads/createThread.php"
		}else{
			echo '<meta http-equiv="refresh" content="0; url=index.php?C=Thread&A=new" />'
		}
	}	

}