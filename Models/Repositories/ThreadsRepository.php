<?php

Class ThreadsController extends Controller {

	private $usersRepository;

	public function __construct(){
		$this->usersRepository = new UsersRepository();
	}

	public function defaultAction(){
		self::listAction();
	} 

	public function listAction(){
		$users = $this->usersRepository->findAll();
		require "Views/Users/list.php";
	}

	public function showAction(){
		if (array_key_exists("UID", $_GET)){
			if (is_numeric($_GET["UID"])){
				$user = $this->usersRepository->findByUID(intval($_GET["UID"]));
				require "Views/Users/show.php";
			}
		}
	}

}
