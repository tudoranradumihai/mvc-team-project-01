<?php

Class Posts{

	public $id;
	public $name;
	public $description;
	public $date;
	public $thread;

	public function __construct($id, $name, $description, $date, $thread){
		$this->id 			= $id;
		$this->name	        = $name;
		$this->description 	= $description;
		$this->date 		= $date;
		$this->thread 	= $thread;
	}

}