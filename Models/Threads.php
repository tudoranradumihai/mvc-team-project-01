<?php

Class Threads{

	public $id;
	public $name;
	public $description;
	public $date;
	public $category;

	public function __construct($id, $name, $description, $date, $category){
		$this->id 			= $id;
		$this->name	        = $name;
		$this->description 	= $description;
		$this->date 		= $date;
		$this->category 	= $category;
	}

}